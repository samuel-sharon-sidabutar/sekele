from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import index
from account.models import Account
from dashboard.views import dashView

# Create your tests here.

class TestHelloWorld(TestCase):
    def test_event_url_is_exist(self):
        c = Client()
        #register dummy user
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()

        #user login
        logged_in = c.login(nomorinduk='12345', password='12345')

        response = c.post(reverse('dashboard:viewDash'), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_event_using_template(self):
        c = Client()

        #register dummy user
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()

        #user login
        logged_in = c.login(nomorinduk='12345', password='12345')

        template = c.get('/dashboard')
        self.assertTemplateUsed(template, 'dashboard.html')

    def test_event_func(self):
        c = Client()

        #register dummy user
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()

        #user login
        logged_in = c.login(nomorinduk='12345', password='12345')

        found_func = resolve('/dashboard')
        self.assertEqual(found_func.func, dashView)

    def test_event_using_template_landing(self):
        template = Client().get('/')
        self.assertTemplateUsed(template, 'index.html')

    def test_event_using_template_index(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_event_url_dashboard_exist(self):
        c = Client()

        #register dummy user
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()

        #user login
        logged_in = c.login(nomorinduk='12345', password='12345')

        response = c.get('/dashboard')
        self.assertEqual(response.status_code, 200)

    def test_if_already_authenticated_exist(self):
        c=Client()
        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        c.login(username='190639', password='12345')
        response = c.get('/')
        self.assertEquals(response.status_code, 302)
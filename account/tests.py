from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import register_view, login_view, logout_view, profile_view
from .models import Account
from .forms import FormRegistrasi, AccountAuthenticationForm
from django.contrib.auth.models import Group


# Create your tests here.

class TestModel(TestCase):

    def test_create_account(self):
        user = Account.objects.create(nomorinduk='1906400274', date_of_birth='2001-10-17')
        user.set_password('12345')
        user.save()
        self.assertEqual(Account.objects.count(), 1)

class TestCreateUser(TestCase):

    def setUp(self):
        self.nomorinduk = "1906400274"
        self.username = "hernowoas"
        self.date_of_birth = "2001-10-17"
        self.password1 = "admintkg15"
        self.password2 = "admintkg15"

        self.test_user = Account.objects.create_user(
            nomorinduk = self.nomorinduk,
            username = self.username,
            date_of_birth = self.date_of_birth,
            password = self.password1,
        )

    def test_create_user(self):
        self.assertIsInstance(self.test_user, Account)

    def test_default_user_is_active(self):
        self.assertTrue(self.test_user.is_active)

    def test_default_user_is_staff(self):
        self.assertFalse(self.test_user.is_staff)

    def test_default_user_is_superuser(self):
        self.assertFalse(self.test_user.is_superuser)


class TestCreateSuperUser(TestCase):

    def setUp(self):
        self.nomorinduk = "1906400274"
        self.username = "hernowoas"
        self.date_of_birth = "2001-10-17"
        self.password1 = "admintkg15"
        self.password2 = "admintkg15"

        self.test_user = Account.objects.create_superuser(
            nomorinduk = self.nomorinduk,
            username = self.username,
            date_of_birth = self.date_of_birth,
            password = self.password1,
        )

    def test_create_user(self):
        self.assertIsInstance(self.test_user, Account)

    def test_default_user_is_active(self):
        self.assertTrue(self.test_user.is_active)

    def test_default_user_is_staff(self):
        self.assertTrue(self.test_user.is_staff)

    def test_default_user_is_superuser(self):
        self.assertTrue(self.test_user.is_superuser)


class TestRegister(TestCase):

    def test_url_register_exist(self):
        response = Client().get('/account/register/')
        self.assertEqual(response.status_code,200)
    
    def test_event_func(self):
        found = resolve('/account/register/')
        self.assertEqual(found.func, register_view)

    def test_template_register_exist(self):
        response = Client().get('/account/register/')
        self.assertTemplateUsed(response, 'account/register.html')


class TestLogin(TestCase):
    
    def test_url_login_exist(self):
        response = Client().get('/account/login/')
        self.assertEqual(response.status_code,200)
    
    def test_event_func(self):
        found = resolve('/account/login/')
        self.assertEqual(found.func, login_view)

    def test_template_login_exist(self):
        response = Client().get('/account/login/')
        self.assertTemplateUsed(response, 'account/login.html')
    
    def test_login_account(self):
        user = Account.objects.create(nomorinduk='1906400274', date_of_birth='2001-10-17')
        user.set_password('12345')
        user.save()
        self.client = Client()
        login = self.client.login(nomorinduk='1906400274', password='12345')
        self.assertTrue(login)

    def test_login_redirect(self):
        user = Account.objects.create(nomorinduk='1906400274', date_of_birth='2001-10-17')
        user.set_password('dummypassword')
        user.save()
        c = Client()
        c.login(nomorinduk='1906400274', password='dummypassword')
        response = c.get('/account/login/')
        self.assertEqual(response.status_code, 302)

    def test_logout_redirect(self):
        user = Account.objects.create(nomorinduk='1906400274', date_of_birth='2001-10-17')
        user.set_password('dummypassword')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)
        c = Client()
        c.login(nomorinduk='1906400274', password='dummypassword')
        response = c.get('/account/logout/')
        self.assertEqual(response.status_code, 302)

class TestDecorator(TestCase):

    def test_allowed_user(self):
        user = Account.objects.create(nomorinduk='1906400274', date_of_birth='2001-10-17')
        user.set_password('dummypassword')
        user.save()
        c = Client()
        c.login(nomorinduk='1906400274', password='dummypassword')
        response = c.get('/announcement/add')
        self.assertEqual(response.status_code, 302)
    

class TestForms(TestCase):

    def test_valid_data(self):
        form  = FormRegistrasi({
            'nomorinduk':'1906400274',
            'username':'hernowoas',
            'date_of_birth':'2001-10-17',
            'password1':'admintkg15',
            'password2':'admintkg15',
        })
        self.assertTrue(form.is_valid())
        user = form.save()
        self.assertEqual(user.nomorinduk, "1906400274")
        self.assertEqual(user.username, "hernowoas")

    def test_form_is_valid(self):
        form_data = {
            'nomorinduk' : '1906400274',
            'password' : 'admintkg15',
        }
        form = AccountAuthenticationForm(form_data)
        response = self.client.post('/account/login/', form_data)
        self.assertEqual(response.status_code, 200)


class TestProfile(TestCase):
    def test_url_profile_exist(self):
        c=Client()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()

        c.login(nomorinduk='190639', password='12345')

        post_response = c.get('/account/profile/')
        self.assertEquals(post_response.status_code, 200)

    def test_complete_profile(self):
        c=Client()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()

        c.login(nomorinduk='190639', password='12345')

        response = c.get('/account/profile/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("My Profile", html_kembalian)
        self.assertIn('Name', html_kembalian)

    def test_template_profile_exist(self):
        c=Client()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()

        c.login(nomorinduk='190639', password='12345')

        response = c.get('/account/profile/')
        self.assertTemplateUsed(response, 'account/profile.html')

class TestSearch(TestCase):

    def test_template_is_exist(self):
        user = Account.objects.create(nomorinduk='1906400274', date_of_birth='2001-10-17')
        user.set_password('dummypassword')
        user.save()
        c = Client()
        c.login(nomorinduk='1906400274', password='dummypassword')
        response = c.get('/account/search/')
        self.assertTemplateUsed(response, 'account/search.html')
    

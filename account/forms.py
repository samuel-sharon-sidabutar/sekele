from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate

from .models import Account

class DateInput(forms.DateInput):
    input_type = 'date'

class FormRegistrasi(UserCreationForm):
    nomorinduk = forms.CharField(max_length=30)
    date_of_birth = forms.DateField(widget=DateInput)

    class Meta:
        model = Account
        fields = [
            'nomorinduk',
            'username',
            'date_of_birth',
            'password1',
            'password2',
        ]

    def clean_nomorinduk(self):
        nomorinduk = self.cleaned_data['nomorinduk']
        try:
            account = Account.objects.get(nomorinduk=nomorinduk)
        except Exception as e:
            return nomorinduk
        raise forms.ValidationError(f"Nomor Induk {nomorinduk} telah terpakai.")
            
    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            account = Account.objects.get(username=username)
        except Exception as e:
            return username
        raise forms.ValidationError(f"Username {username} telah terpakai.")

class AccountAuthenticationForm(forms.ModelForm):
    password = forms.CharField(label="Password", widget=forms.PasswordInput)
    class Meta:
        model = Account
        fields = [
            "nomorinduk",
            "password"
        ]

    def clean(self):
        if self.is_valid():
            nomorinduk = self.cleaned_data['nomorinduk']
            password = self.cleaned_data['password']
            if not authenticate(nomorinduk=nomorinduk, password=password):
                raise forms.ValidationError("Invalid Login")



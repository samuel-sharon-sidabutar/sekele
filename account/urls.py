from django.urls import path
from account.views import register_view, login_view, logout_view, profile_view, search_view, search_page_view


app_name = 'account'

urlpatterns = [
    path('login/', login_view, name="login"),
    path('register/', register_view, name="register"),
    path('logout/', logout_view, name='logout'),
    path('profile/', profile_view, name='profile'),
    path('search/', search_page_view, name='search'),
    path('data/', search_view, name='data')
]
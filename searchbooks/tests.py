from django.test import TestCase, Client
from account.models import Account

# Create your tests here.
class Test(TestCase):
    def test_url_exist(self):
        c=Client()
        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        c.login(username='190639', password='12345')
        response = c.get('/search/')
        self.assertEquals(response.status_code, 200)

    def test_text_complete(self):
        c=Client()
        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        c.login(username='190639', password='12345')
        response = c.get('/search/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Search for course books", html_kembalian)

    def test_template_exist(self):
        c=Client()
        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        c.login(username='190639', password='12345')
        response = c.get('/search/')
        self.assertTemplateUsed(response, 'searchbooks/search.html')

    def test_url_jsongetter_exist(self):
        response = Client().get('/search/search?search_key=Harry Potter')
        self.assertEquals(response.status_code, 200)    

    def test_jsonresponse_is_correct(self):
        response = Client().get('/search/search?search_key=')
        response_content = response.content
        self.assertJSONEqual(
            response_content,
            {
            "error": {
                "code": 400,
                "message": "Missing query.",
                "errors": [
                {
                    "message": "Missing query.",
                    "domain": "global",
                    "reason": "queryRequired",
                    "location": "q",
                    "locationType": "parameter"
                }
                ]
            }
            }
        )
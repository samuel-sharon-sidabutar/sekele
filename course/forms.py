from django import forms
from .models import Week

class FormAddWeek(forms.ModelForm):
    class Meta:
        model = Week
        fields = {
            'topic',
            'google_meet_link',
            'homework_drive',
            'quiz_forms',
            'topic_sheets',
        }
        widgets = {
            'google_meet_link' : forms.TextInput(attrs={'class': 'form-control'}),
            'homework_drive' : forms.TextInput(attrs={'class': 'form-control'}),
            'quiz_forms' : forms.TextInput(attrs={'class': 'form-control'}),
            'topic_sheets' : forms.TextInput(attrs={'class': 'form-control'}),
            'topic' : forms.TextInput(attrs={'class': 'form-control'}),
        }

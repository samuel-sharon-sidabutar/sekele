# Generated by Django 3.1.1 on 2020-12-31 04:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0007_auto_20201231_1056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='syllabus',
            field=models.CharField(default='https://docs.google.com/document/d/1ylU3ZOxiG38ORD9em_8GTQ6ZacgFwQ96KQOTbpygZTI/edit?usp=sharing', max_length=256),
        ),
    ]

from django.contrib import admin
from .models import Course, Week, Enrollment

# Register your models here.
admin.site.register(Course)
admin.site.register(Week)
admin.site.register(Enrollment)
# admin.site.register(Student)
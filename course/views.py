from django.forms.forms import Form
from course.forms import FormAddWeek
from django.shortcuts import render, redirect
from django.views.generic import ListView
from .models import Course, Week
from django.http import HttpResponseRedirect, JsonResponse
from .forms import FormAddWeek
from .models import Course, Week, Enrollment
from django.db.models import Q
from account.models import Account
from django.contrib.auth.models import Group
from account.decorators import allowed_user, unauthenticated_user
from django.contrib.auth.decorators import login_required
import json

# Create your views here.
@login_required(login_url='/account/login/')
@allowed_user(allowed_roles=['lecturer'])
def add_week(request, pk):
    course = Course.objects.get(pk=pk)
    form = FormAddWeek(request.POST or None)
    User = request.user
    Role = User.groups.values_list('name',flat = True)
    Roles = list(Role)
    enroll = Enrollment.objects.get(member=User, course=course)
    if (request.method == 'GET'):
        response = {
            'form' : form,
            'User' : User,
            'Roles':Roles,
        }
        return render(request, 'course/add-week.html', response)
    elif (form.is_valid() and request.method == 'POST'):
        topic               = form.cleaned_data['topic']
        google_meet_link    = form.cleaned_data['google_meet_link']
        homework_drive      = form.cleaned_data['homework_drive']
        quiz_forms          = form.cleaned_data['quiz_forms']
        topic_sheets        = form.cleaned_data['topic_sheets']
        participant         = Week.objects.create(topic = topic, google_meet_link = google_meet_link, homework_drive = homework_drive, quiz_forms = quiz_forms, topic_sheets = topic_sheets, course = course)
        participant.save()
        return HttpResponseRedirect('/course/detail/' + str(enroll.pk) + '/') #tambahin pk course

@login_required(login_url='/account/login/')
@allowed_user(allowed_roles=['lecturer'])
def update_week(request, pk):
    week = Week.objects.get(pk = pk)
    course = week.course
    a = course.pk
    User = request.user
    Role = User.groups.values_list('name',flat = True)
    Roles = list(Role)
    enroll = Enrollment.objects.get(member=User, course=course)
    data = {
        'topic'             : week.topic,
        'google_meet_link'  : week.google_meet_link,
        'homework_drive'    : week.homework_drive,
        'quiz_forms'        : week.quiz_forms,
        'topic_sheets'      : week.topic_sheets,
    }

    form = FormAddWeek(request.POST or None, initial=data, instance=week)
    context = {
        'form' : form,
        'User' : User,
        'Roles':Roles,
    }
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/course/detail/' + str(enroll.pk) + '/' ) #tambahin pk course
    
    return render(request, 'course/add-week.html', context) 

@login_required(login_url='/account/login/')
def course(request):
    course = Course.objects.all()
    User = request.user
    context={
        'course':course,
        'User' : User,
    }

    return render(request, 'course/list-matkul.html', context)

@login_required(login_url='/account/login/')
def enroll(request, id_course):
    User = request.user
    course_obj = Course.objects.get(id=id_course)
    member_obj = User

    course_obj.member.add(member_obj)    
    course_obj.save()

    return redirect('/course/success/')

@login_required(login_url='/account/login/')
def unenroll(request, id_course):

    course_obj = Course.objects.get(id=id_course)
    member_obj = request.user

    Enrollment.objects.filter(member=member_obj, course=course_obj).delete()

    return redirect('/dashboard')

@login_required(login_url='/account/login/')
def success(request):
    User = request.user
    context={
        'User' : User,
    }
    return render(request, 'course/success.html', context)

@login_required(login_url='/account/login/')
def coursepage(request, id_enroll):
    User = request.user
    list_course = Enrollment.objects.filter(member=User)
    enroll_obj = Enrollment.objects.get(id=id_enroll)
    course_target = Course.objects.get(name=enroll_obj.course)
    week_obj = Week.objects.filter(course=course_target)
    Role = User.groups.values_list('name',flat = True)
    Roles = list(Role)
    # lecturer = Enrollment.objects.filter(member=Group.objects.get(name='lecturer'), course=course_target)
    lecturer = []
    # for i in Account.objects.all():
    #     if 'lecturer' in list.groups.values_list('name',flat = True) and :
    #         lecturer.append(i)
    for i in Enrollment.objects.all():
        if 'lecturer' in i.member.groups.values_list('name',flat = True) and i.course == course_target:
            lecturer.append(i)
    context={
        'listCourse':list_course,
        'title':'Enrolled Course',
        'course_obj':course_target,
        'week_obj':week_obj,
        'User' : User,
        'Roles':Roles,
        'lecturer':lecturer
    }

    return render(request, "course/course-detail.html", context)

@login_required(login_url='/account/login/')
def search(request):
    searchCourse = request.GET['q']
    objCourse = Course.objects.filter(name__icontains=searchCourse)
    data = objCourse.values()
    return JsonResponse(list(data), safe=False)


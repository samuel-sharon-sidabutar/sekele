from django.db import models
from django import forms
# from django.contrib.auth.models import User
from sekele.settings import AUTH_USER_MODEL as User

# Create your models here.
class AnnouncementModel(models.Model):
    judulAn             = models.CharField(max_length = 100)
    publisherAn         = models.ForeignKey(User,on_delete=models.CASCADE, null=True)
    isiAn               = models.TextField()
    postedAn            = models.DateTimeField(auto_now_add=True)
    lastEditAn          = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.judulAn
    
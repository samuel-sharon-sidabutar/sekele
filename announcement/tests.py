from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import AnnAdd, AnnDelete, AnnEdit, AnnView
from .models import AnnouncementModel
from .apps import AnnouncementConfig
from django.utils import timezone
from account.models import Account as User
from django.contrib.auth.models import Group

#MODEL-ANNOUNCEMENT
class TestModelAnnouncement(TestCase):

    def test_add_model(self):
        Ann = AnnouncementModel(judulAn='judul',isiAn='isi')
        Ann.save()
        self.assertEqual(AnnouncementModel.objects.all().count(),1)

    def test_str_model(self):
        Ann = AnnouncementModel(judulAn='judul',isiAn='isi')
        Ann.save()
        self.assertEqual(str(Ann),Ann.judulAn)

#ADD-ANNOUNCEMENT
class TestAddAnnouncement(TestCase):

    def test_event_url_is_exist(self):
        c = Client()
        user = User.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)
        c.login(nomorinduk='12345', password='12345')

        response = c.get('/announcement/add')
        self.assertEqual(response.status_code,200)

    def test_event_func(self):
        found = resolve('/announcement/add')
        self.assertEqual(found.func, AnnAdd)
    
    def test_template_used(self):
        c = Client()
        user = User.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)
        c.login(nomorinduk='12345', password='12345')

        response = c.get('/announcement/add')
        self.assertTemplateUsed("announcement/formAn.html")

    def test_post_is_exist(self):
        user = User.objects.create(nomorinduk='190639',date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save() 
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)
        c = Client()
        logged_in = c.login(username='190639',password='12345')

        response = c.post(
            '/announcement/add', data={'judulAn':'judul','isiAn':'isi'}
        )
        self.assertEqual(response.status_code, 302)

#VIEW-ANNOUNCEMENT
class TestViewAnnouncement(TestCase):

    def test_annview_is_work(self):
        Ann = AnnouncementModel(judulAn='judul',isiAn='isi')
        Ann.save()

        post_response = self.client.post(reverse('announcement:viewAn', args=(Ann.id,)), follow=True)
        self.assertEqual(post_response.status_code,200)

    def test_event_func(self):
        Ann = AnnouncementModel(judulAn='judul',isiAn='isi')
        Ann.save()

        found = resolve('/announcement/1/view')
        self.assertEqual(found.func, AnnView)

    def test_template_used(self):
        c = Client()
        user = User.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)
        c.login(nomorinduk='12345', password='12345')

        Ann = AnnouncementModel(judulAn='judul',isiAn='isi')
        Ann.save()

        response = c.get('/announcement/1/view')
        self.assertTemplateUsed("announcement/viewAn.html")

#EDIT-ANNOUNCEMENT
class TestEditAnnouncement(TestCase):

    def test_success_editann(self):
        Ann = AnnouncementModel(judulAn='judul',isiAn='isi')
        Ann.save()
        c = Client()
        user = User.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)
        c.login(nomorinduk='12345', password='12345')

        get_response = c.get('/announcement/1/edit')
        post_response = c.post('/announcement/1/edit',
                    data={'judulAn':'judulganti','isiAn':'isiganti'})
        
        self.assertEqual(get_response.status_code,200)
        self.assertEqual(post_response.status_code,302)

    def test_event_func(self):
        Ann = AnnouncementModel(judulAn='judul',isiAn='isi')
        Ann.save()

        found = resolve('/announcement/1/edit')
        self.assertEqual(found.func, AnnEdit)

    def test_template_used(self):
        c = Client()
        user = User.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)
        c.login(nomorinduk='12345', password='12345')

        Ann = AnnouncementModel(judulAn='judul',isiAn='isi')
        Ann.save()

        response = c.get('/announcement/1/edit')
        self.assertTemplateUsed("announcement/formAn.html")

#DELETE-ANNOUNCEMENT
class TestDeleteAnnouncement(TestCase):

    def test_delete_model(self):
        Ann = AnnouncementModel(judulAn='judul',isiAn='isi')
        Ann.save()
        user = User.objects.create(nomorinduk='190639',date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)

        c = Client()
        logged_in = c.login(username='190639',password='12345')
        post_response = c.post(reverse('announcement:deleteAn', args=(Ann.id,)), follow=True)
        self.assertEqual(post_response.status_code,200)

    def test_event_func(self):
        Ann = AnnouncementModel(judulAn='judul',isiAn='isi')
        Ann.save()

        found = resolve('/announcement/1/delete')
        self.assertEqual(found.func,AnnDelete)
